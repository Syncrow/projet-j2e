# Installation 

installer JAVA11 et la choisir comme version pour lancer le programme.

installer MySQL

Se connecter en root à MySQL
​   `sudo mysql`

Créer un utilisateur 

​	`CREATE USER 'springuser'@'localhost' IDENTIFIED BY '1234';`

Donner tout les droits à l'utilisateur

​	`GRANT ALL PRIVILEGES ON *.* TO 'springuser'@'localhost' IDENTIFIED BY '1234';`

Se déconnecter

​	`\q`

Se reconnecter en tant que l'utilisateur créer

​	`mysql -u springuser -p`

Créer une database

​	`CREATE DATABASE tp4;`



# Utilisation

Pour utiliser l'application, taper `./mvnw spring-boot:run` l'application va compiler et ensuite sera accessible sur `http://localhost:8080`. 

Pour commencer il faut ajouter un événement pour pouvoir y ajouter ensuite des participants.

 

